﻿using UnityEngine;
using UnityEngine.XR.WSA.Input;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.InputModule.Tests;
using System.Collections.Generic;
using System.Collections;

namespace Academy.HoloToolkit.Unity
{
    /// <summary>
    /// GestureManager contains event handlers for subscribed gestures.
    /// </summary>
    public class GestureManager : MonoBehaviour, IInputClickHandler
    {

        private GestureRecognizer gestureRecognizer;

        private Animator animator = null;

        private int fromStates(SpawnItems.States state)
        {
            switch(state)
            {
                case SpawnItems.States.INTENSE_ANGER:
                    return 0;
                case SpawnItems.States.MODERATE_ANGER:
                    return 1;
                case SpawnItems.States.LOW_ANGER:
                    return 2;
                case SpawnItems.States.NEUTRAL:
                    return 3;
                case SpawnItems.States.LOW_JOY:
                    return 4;
                case SpawnItems.States.MODERATE_JOY:
                    return 5;
                case SpawnItems.States.INTENSE_JOY:
                    return 6;
                default:
                    return 100; // UNDEFINED STATE
            }
        }

        private SpawnItems.States toState(int state)
        {
            switch(state)
            {
                case 0:
                    return SpawnItems.States.INTENSE_ANGER;
                case 1:
                    return SpawnItems.States.MODERATE_ANGER;
                case 2:
                    return SpawnItems.States.LOW_ANGER;
                case 3:
                    return SpawnItems.States.NEUTRAL;
                case 4:
                    return SpawnItems.States.LOW_JOY;
                case 5:
                    return SpawnItems.States.MODERATE_JOY;
                case 6:
                    return SpawnItems.States.INTENSE_JOY; 
                default:
                    return SpawnItems.States.UNDEFINED_STATE;
            }
        }

        // matrix for equation:
        // line: Avatar's expression in the previous trial
        // column: Participant response's in the previous trial
        // access the matrix: matrix[2,3]
        private int[,] matrix = new int[7, 7]
        {
            {0, 6, 5, 4, 3, 2, 1 },
            {2, 1, 0, 6, 5, 4, 3 },
            {4, 3, 2, 1, 0, 6, 5 },
            {6, 5, 4, 3, 2, 1, 0 },
            {1, 0, 6, 5, 4, 3, 2 },
            {3, 2, 1, 0, 6, 5, 4 },
            {5, 4, 3, 2, 1, 0, 6 }
        }; 



        //private void loadPrefabsByName(string name)
        //{
        //    GameObject[] itemTypes = SpawnItems.Instance.ItemTypes;
        //    for (int i = 0; i < itemTypes.Length; i++)
        //    {
        //        //if (itemTypes[i].name == name)
        //        {
        //            int contor = 0;
        //            while (contor < SpawnItems.Instance.Number)
        //            {
        //                GameObject gameObject = itemTypes[i];

        //                Vector3 position = new Vector3(
        //                    Random.Range(-2, 2),
        //                    Random.Range(-1f, 1.5f),
        //                    Random.Range(-2, 2));

        //                GameObject instantiatedObject = Instantiate(gameObject, position, this.transform.rotation);
        //                Vector3 scale = new Vector3();
        //                float genScale = Random.Range(0.4f, 0.9f);
        //                scale.x = genScale;
        //                scale.y = genScale;
        //                scale.z = genScale;
        //                instantiatedObject.transform.localScale = scale;
        //                contor++;
        //            }
        //        }
        //    }
        //}

        //private void destroyPrefabsByName(string name, GameObject[] objects)
        //{
        //    for (int i = 0; i < objects.Length; i++)
        //    {
        //        if (objects[i].name.Contains(name))
        //        {
        //            Destroy(objects[i]);
        //        }
        //    }
        //}
        void Start()
        {

            InputManager.Instance.PushFallbackInputHandler(this.gameObject);
            gestureRecognizer = new GestureRecognizer();
            gestureRecognizer.SetRecognizableGestures(GestureSettings.Tap);

            gestureRecognizer.Tapped += (args) =>
            {
                //GameObject focusedObject = this.gameObject;

                //if (focusedObject != null)
                //{
                //    //focusedObject.SendMessage("OnInputClicked");
                    
                //    focusedObject.transform.Rotate(20,-60, 5);
                //}
            };

            gestureRecognizer.StartCapturingGestures();
            animator = SpawnItems.Instance.Avatar.GetComponent<Animator>();
        }

        void OnDestroy()
        {
            gestureRecognizer.StopCapturingGestures();
        }

        private SpawnItems.States choice(string nameOfGameObject)
        {
            if (nameOfGameObject.Contains("0"))
                return SpawnItems.States.INTENSE_ANGER;
            else if (nameOfGameObject.Contains("1"))
                return SpawnItems.States.MODERATE_ANGER;
            else if (nameOfGameObject.Contains("2"))
                return SpawnItems.States.LOW_ANGER;
            else if (nameOfGameObject.Contains("3"))
                return SpawnItems.States.NEUTRAL;
            else if (nameOfGameObject.Contains("4"))
                return SpawnItems.States.LOW_JOY;
            else if (nameOfGameObject.Contains("5"))
                return SpawnItems.States.MODERATE_JOY;
            else if (nameOfGameObject.Contains("6"))
                return SpawnItems.States.INTENSE_JOY;
            else
                return SpawnItems.States.UNDEFINED_STATE;
        }




  
        public void OnInputClicked(InputClickedEventData eventData)
        {
            Debug.Log("eventData.selectedObject=" + eventData.selectedObject +
                "/eventData.currentInputModule" + eventData.currentInputModule + 
                "/eventData.InputSource=" + eventData.InputSource + "/eventData.pressType" + eventData.PressType + 
                "/eventData.SourceId" + eventData.SourceId + "/eventData.used" + eventData.used);
            if (eventData.selectedObject != null && this.gameObject.name != eventData.selectedObject.name)
                return;
            Debug.Log("Trial " + SpawnItems.Instance.countTrial.ToString()); 
            // Write your gesture code here
            int linie, coloana;
            SpawnItems.States result;
            string result_anim = "";
            if (SpawnItems.Instance.isFirstTrial)
            {
                //Debug.Log("Inainte de trial-ul " + SpawnItems.Instance.countTrial.ToString() + ", avatar_expr_prev=" + SpawnItems.Instance.avatar_expression_in_the_previous_trial.ToString() + 
                 //   ", previous_response=" + SpawnItems.Instance.participants_response_in_the_previous_trial.ToString());
                SpawnItems.Instance.participants_response_in_the_previous_trial = choice(this.gameObject.name);
                Debug.Log("Ai ales this.gameObject.name= " + this.gameObject.name);
                linie = fromStates(SpawnItems.Instance.avatar_expression_in_the_previous_trial);
                coloana = fromStates(SpawnItems.Instance.participants_response_in_the_previous_trial);
                Debug.Log("linie=" + linie + ",coloan=" + coloana);
                result = toState(matrix[linie, coloana]);
                // actualizeaza avatarul dupa result (animatia)
                result_anim = (fromStates(result) + 1).ToString() + "_" + (fromStates(result) + 1).ToString();
                
                animator.Play(result_anim);
                SpawnItems.Instance.avatar_expression_in_the_previous_trial = result;
                Debug.Log("Dupa trial-ul " + SpawnItems.Instance.countTrial.ToString() + ", avatar_expr_prev(noul result)=" + SpawnItems.Instance.avatar_expression_in_the_previous_trial.ToString() +
                    ", previous_response(alegerea facuta la acest trial)=" + SpawnItems.Instance.participants_response_in_the_previous_trial.ToString());
                SpawnItems.Instance.isFirstTrial = false;
                SpawnItems.Instance.deactivateOrActivateObects(false);
                SpawnItems.Instance.watch.Start();
                Debug.Log("Am pornit cronometrul pentru Trial-ul " + SpawnItems.Instance.countTrial.ToString());
                SpawnItems.Instance.countTrial++;
                return;

            }
            Debug.Log("Inainte de trial-ul " + SpawnItems.Instance.countTrial.ToString() + ", avatar_expr_prev=" + SpawnItems.Instance.avatar_expression_in_the_previous_trial.ToString() +
            ", previous_response=" + SpawnItems.Instance.participants_response_in_the_previous_trial.ToString());
            linie = fromStates(SpawnItems.Instance.avatar_expression_in_the_previous_trial);
            coloana = fromStates(choice(this.gameObject.name));
            Debug.Log("Ai ales this.gameObject.name= " + this.gameObject.name);
            if (toState(coloana) == SpawnItems.Instance.participants_response_in_the_previous_trial)
            {
                // nu permitem acelasi raspuns, repetam trial-ul (deci nu incrementam trial-ul)
                // avertizare utilizator ca a dat acelasi raspuns si ca trebuie sa repete alegerea pentru trial-ul curent
                // momentan facem doar return, aceeasi semnificatie
                Debug.Log("Ai dat acelasi raspuns consecutiv, nu este ok! Cronometrul este pornit? " + SpawnItems.Instance.watch.IsRunning.ToString());
                while (SpawnItems.Instance.watch.IsRunning) ;
                return;
            }
            Debug.Log("linie=" + linie + ",coloana=" + coloana + ",obiect=" + this.gameObject.name);
            result = toState(matrix[linie, coloana]);
            // actualizeaza avatarul dupa result (animatia)
            result_anim = (fromStates(result)+1).ToString() + "_" + (fromStates(result)+1).ToString();
            animator.Play(result_anim);
            SpawnItems.Instance.participants_response_in_the_previous_trial = toState(coloana);
            SpawnItems.Instance.avatar_expression_in_the_previous_trial = result;
            Debug.Log("Dupa trial-ul " + SpawnItems.Instance.countTrial.ToString() + ", avatar_expr_prev(noul result)=" + SpawnItems.Instance.avatar_expression_in_the_previous_trial.ToString() +
                    ", previous_response(alegerea facuta la acest trial)=" + SpawnItems.Instance.participants_response_in_the_previous_trial.ToString());
            SpawnItems.Instance.deactivateOrActivateObects(false); // deactivate objects
            SpawnItems.Instance.watch.Start();
            Debug.Log("Am pornit cronometrul pentru Trial-ul " + SpawnItems.Instance.countTrial.ToString());

            SpawnItems.Instance.countTrial++;
            //MicStreamDemo.Instance.downloadMP3("Sunt " + this.gameObject.name + " si m-ai accesat!");
            //this.BroadcastMessage("downloadMP3", "Te salut si eu, drag utilizator!");
            //Debug.Log("Da ma, ai dat click pe " + this.gameObject.name + " dar asta nu face broadcast calumea!!!! :))");
            //GameObject[] objects = SceneManager.GetActiveScene().GetRootGameObjects();
            //if(this.gameObject.name.Contains("Sphere"))
            //{
            //    destroyPrefabsByName("Sphere", objects);
            //    if (this.gameObject.name.Contains("Box"))
            //    {
            //        loadPrefabsByName("Box");
            //    }
            //    else if (this.gameObject.name.Contains("ford"))
            //    {
            //        loadPrefabsByName("ford_GT");
            //    }
            //    else if (this.gameObject.name.Contains("Fruit"))
            //    {
            //        loadPrefabsByName("Fruits");
            //    }
            //}
            //else
            //{
            //    destroyPrefabsByName(this.gameObject.name, objects);
            //    SpawnItems.Instance.SpawnManyItems();
            //}
        }
    }
}