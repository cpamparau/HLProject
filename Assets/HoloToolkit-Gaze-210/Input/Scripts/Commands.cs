﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Commands : MonoBehaviour
{
    Vector3 originalPosition;

    // Use this for initialization
    void Start()
    {
        // Grab the original local position of the sphere when the app starts.
        originalPosition = this.transform.localPosition;
    }

    // Called by GazeGestureManager when the user performs a Select gesture
    void OnStart()
    {
        GameObject[] objects = SceneManager.GetActiveScene().GetRootGameObjects();
        if (objects.Length > 0)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                objects[i].SetActive(true);
            }
        }
    }

    // Called by SpeechManager when the user says the "Reset world" command
    void OnStop()
    {
        GameObject[] objects = SceneManager.GetActiveScene().GetRootGameObjects();
        if (objects.Length > 0)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                objects[i].SetActive(false);
            }
        }
    }
}