﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using UnityEditor;
using UnityEngine;
using HoloToolkit.Unity.InputModule;

namespace HoloToolkit.Unity.InputModule
{
    public class SpeechInputSourceEditor :  MonoBehaviour, ISpeechHandler
    {
        
        public void OnSpeechKeywordRecognized(SpeechEventData eventData)
        {
            update(eventData.RecognizedText);
        }

        public void update(string command)
        {
            switch(command)
            {
                case "go":
                    //transform.position += Vector3.forward * 1.2f;
                    this.gameObject.GetComponent<Animator>().StopPlayback();
                    this.gameObject.GetComponent<Animator>().enabled = false;
                    this.gameObject.transform.position += Vector3.forward * 1.5f;
                    break;
                case "come":
                    //transform.position += Vector3.forward / 1.2f;
                    this.gameObject.GetComponent<Animator>().enabled = true;
                    break;
            }
        }
    }
}
