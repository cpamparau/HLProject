﻿using System.Collections;
using System.Collections.Generic;
using Academy.HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnItems : Singleton<SpawnItems> {

    //public GameObject[] ItemTypes;

    public SpriteRenderer[] faces;

    public MeshRenderer Table;

    public MeshRenderer Chair; 

    public GameObject Avatar;

    public System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

    public int Number;

    public bool isFirstTrial = true;

    public int countTrial = 0;

    public enum States
    {
        INTENSE_ANGER = 0,
        MODERATE_ANGER = 1,
        LOW_ANGER = 2,
        NEUTRAL = 3,
        LOW_JOY = 4,
        MODERATE_JOY = 5,
        INTENSE_JOY = 6,
        UNDEFINED_STATE = 100
    };

    public void deactivateOrActivateObects(bool deactivate)
    {
        for (int i=0;i<faces.Length;i++)
            faces[i].enabled = deactivate;
    }

    public States avatar_expression_in_the_previous_trial = States.INTENSE_ANGER; // used for accessing the line of the below matrix

    public States participants_response_in_the_previous_trial = States.INTENSE_ANGER; // used for accessing the column of the below matrix

    // Use this for initialization
    void Start ()
    {
        for (int i=0;i<7;i++)
            faces[i].enabled = true;
        Chair.enabled = true;
        Table.enabled = true;
        Avatar = Instantiate(Avatar);
        
        //SpawnManyItems();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (watch.ElapsedMilliseconds >= 2000)
        {
            watch.Stop();
            watch.Reset();
            //Debug.Log("Am oprit cronometrul pentru Trial-ul " + SpawnItems.Instance.countTrial.ToString());
            deactivateOrActivateObects(true);
        }
    }

    //public void SpawnManyItems()
    //{
    //    int count = 0;
    //    while ( count < Number)
    //    {

    //        GameObject gameObject = ItemTypes[0];
    //        Vector3 position = new Vector3(0, -3+count, count+2);
    //            //Random.Range(-3, 4),
    //            //Random.Range(-1f, 1.5f),
    //            //Random.Range(-3, 4));
            
    //        GameObject instantiatedObject = Instantiate(gameObject, position, this.transform.rotation);
    //        instantiatedObject.transform.rotation *= Quaternion.Euler(0, 180f, 0);
    //        Vector3 scale = new Vector3();
    //        float genScale = 0.9f;
    //        scale.x = genScale;
    //        scale.y = genScale;
    //        scale.z = genScale;
    //        instantiatedObject.transform.localScale = scale;
    //        //Debug.Log("Am instantiat obiectul " + count.ToString());
    //        count++;
    //    }
    //}
}
